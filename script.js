
// Esta es una funcion declarada que recibe como parametro un objeto db y un objeto contacto
// la funcion guarda en el local storage el contacto
// además refresca la pantalla para que se puedan seguir ingresando datos más rápido
const guardarContacto = (db, contacto) => {
    db.setItem(contacto.id, JSON.stringify(contacto))
    window.location.href = 'index.html' 

};

// Esta funcion obtiene los elementos (nombre, telefono, dirección)
function agregarContacto() {
    const nombre = document.getElementById('nombre');
    const telefono = document.getElementById('telefono');
    const direccion= document.getElementById('direccion');

    if (nombre.value.trim() !== "" && telefono.value.trim() !== "" && direccion.value.trim() !== ""){

        const nombreContacto = nombre.value;
        const telefonoContacto = telefono.value;
        const direccionContacto = direccion.value;

        const contacto = {
            id: Date.now(),
            nombre: nombreContacto,
            telefono: telefonoContacto,
            direccion: direccionContacto,
        };
        guardarContacto(localStorage, contacto);
    } else {
   
    
    };
};

// En esta función toma todos los datos guardados en el Local Storage y los muestra en una tabla en la pantalla
//  Cada dato se convierte en una fila de la tabla, donde cada columna muestra un valor del dato
//  Utiliza comillas invertidas para hacer la escritura del código más clara.
function mostrarContactos() {
    const contactos = Object.values(localStorage)
    const tablaContactos = document.getElementById('contactos')

    for (const contacto of contactos) {
        const contactoObj = JSON.parse(contacto)
        const tr = document.createElement('tr')
        tr.innerHTML = `
            <td class="text-center">${contactoObj.nombre}</td>
            <td class="text-center">${contactoObj.telefono}</td>
            <td class="text-center">${contactoObj.direccion}</td>
        `
        tr.innerHTML += `
            <td class="text-center">
                <a class="btn btn-light btn-sm" data-id="${contactoObj.id}" onclick="eliminarContacto.call(this)"><img src="./asset/img/delete.png" alt="Borrar"></a>
            </td>
        `
        tablaContactos.appendChild(tr)
    };
};

// Esta funcion recibe el id del contacto y lo elimina del local storage
function eliminarContacto() {
    const id = this.getAttribute('data-id')
    localStorage.removeItem(id)
    window.location.href = 'index.html'
};

// Esta funcion recibe el id del contacto y lo busca en el local storage luego lo devuelve para mostrarlo por pantalla
function buscarContacto() {
    // Obtiene el valor del input de búsqueda 
    const input = document.getElementById('buscar');
    const filtro = input.value.toUpperCase();

    // Obtiene la tabla y las filas de la tabla
    const tabla = document.getElementById('contactos');
    const tr = tabla.getElementsByTagName('tr');

    // Itera sobre cada fila de la tabla
    for (let i = 0; i < tr.length; i++) {
        // Obtiene la primera celda de la fila
        let td = tr[i].getElementsByTagName('td')[0];

        // Si hay una celda
        if (td) {
            // Obtiene el texto de la celda ¿s
            let textValue = td.textContent || td.innerText;

            // Verifica si el texto de la celda incluye el filtro de búsqueda
            if (textValue.toUpperCase().indexOf(filtro) > -1) {
                // Si hay coincidencia, muestra la fila
                tr[i].style.display = '';
            } else {
                // Si no hay coincidencia, oculta la fila
                tr[i].style.display = 'none';
            }
        }
    }
}

// Esta funcion limpia los campos de nombre, telefono y dirección
// setea los valores en vacio.
function limpiarTodo() {
    document.getElementById('nombre').value = '';
    document.getElementById('telefono').value = '';
    document.getElementById('direccion').value = '';
};


// Esta funcion limpia todo el local storage y redirige a la pagina principal.
// ademas muestra un mensaje de alerta para confirmar la eliminación
function borrarTodo() {
    if (localStorage.length === 0) {
        return;
    }

    if (confirm("¿Estás seguro? Después no habrá vuelta atrás")) {
        localStorage.clear();
        setTimeout(() => { window.location.href = 'index.html'; }, 2000);
    }
}

// Esta funcion se ejecuta cuando el documento esta cargado
// llama a la funcion mostrarContactos para mostrar los contactos en la tabla.
document.addEventListener('DOMContentLoaded', (event) => {
    mostrarContactos();
    totalContactos();
});